#!/usr/bin/env python
import rospy
import numpy as np
#from std_msgs.msg import String
#import geometry_msgs.msg
from sensor_msgs.msg import JointState
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import tf2_ros
import robot_utils as utils
from ddpg import DDPG

NUM_AXIS = 6
currentJointState = JointState()
def joint_listener_callback(data):
    global currentJointState
    currentJointState = data
    
def get_reward(start_point = np.array([0.665, 0.164, 0.624]), diameter = 0.25, 
               trans = tf2_ros.TransformStamped(), prev_trans = tf2_ros.TransformStamped()):
    #circle in plane parallerl to YZ is assumed, start point on 12 o'clocks
    #robot should keep tool orientation
    center_point = start_point - [0,0,diameter/2]
    r = diameter/2
    # reward function is - radius diff in mm - rot diff in degrees + angular_speed
    pos = utils.pos(trans)
    pos_prev = utils.pos(prev_trans)
    shift = pos - center_point
    shift_prev = pos_prev - center_point
    radius_diff = 1000*(np.abs(shift[0])+np.abs(r - np.linalg.norm(shift[1:])))
    radius_diff_prev = 1000*(np.abs(shift_prev[0])+np.abs(r - np.linalg.norm(shift_prev[1:]))) 
    rot_diff = (360.0/(2*np.pi)) * utils.quaternion_distance(utils.rot(trans), 
                utils.start_tool0_orientation())
    prev_pos = utils.pos(prev_trans)
    #angular vel 
    cp = pos - center_point #center -> point
    cpp = prev_pos - center_point #center -> previous point
    csp = start_point - center_point #center -> start point 
    delta_r = np.abs(radius_diff_prev) - np.abs(radius_diff)
    ncp, ncpp, ncsp = [x / np.linalg.norm(x) for x in [cp, cpp, csp]] #normalization
    angle = np.arccos(np.dot(ncsp, ncp)) if pos[1] > center_point[1] else np.pi + np.arccos(-np.dot(ncsp, ncp)) 
    angle_prev = np.arccos(np.dot(ncsp, ncpp)) if pos[1] > center_point[1] else np.pi + np.arccos(-np.dot(ncsp, ncpp)) 
    omega = angle - angle_prev
    if np.abs(omega) > 1.0: # 2pi -> 0 transitiom
        omega = omega + 2* np.pi if omega < 0 else omega - 2* np.pi
    omega *=50 #scale                
    #print 'Radius_diff: {}\n rot_diff: {}\nomega: {}'.format(radius_diff, rot_diff, omega)
    #reward = omega - radius_diff - rot_diff
    reward = 50-radius_diff+delta_r
    #print 'FP:', 0.01 * reward
    #print 'DR:', 0.01 * delta_r
    return 0.01*reward #scaled

def vel_command(vel = np.zeros(NUM_AXIS), time_step = 0.1):
    #function generates command to feed Joint Trajectory Controller
    #vel - velocity in rad/s for each joint of robot
    global currentJointState
    current_state = dict(zip(currentJointState.name, currentJointState.position))
    #set joints in proper order:
    current_state = np.array([current_state[joint] for joint in utils.ur10_joint_names()])
    target_state = current_state + time_step * vel
    trajectory_point = JointTrajectoryPoint()
    command = JointTrajectory()
    trajectory_point.positions = list(target_state)
    trajectory_point.time_from_start = rospy.Duration(time_step)
    command.joint_names = utils.ur10_joint_names()
    command.points = [trajectory_point]
    return command
def home_command():
    #return command to move robot to home position
    command = JointTrajectory()
    position = utils.start_joint_pos()
    start_point = JointTrajectoryPoint()
    start_point.positions = list(position)
    start_point.time_from_start = rospy.Duration(1)
    command.joint_names = utils.ur10_joint_names()
    command.points = [start_point]
    return command
def random_command():
    return vel_command(2 * np.random.rand(NUM_AXIS)-1)
    
    

if __name__ == '__main__':
    #=========ROS communication
    rospy.init_node('controller', anonymous=True)
    #listeners
    rospy.Subscriber('/joint_states', JointState, joint_listener_callback)
    tfBuffer = tf2_ros.Buffer()
    tool_listener = tf2_ros.TransformListener(tfBuffer)
    #publisher 
    pub = rospy.Publisher('/arm_controller/command', JointTrajectory, queue_size=1)    
    time_step = 1
    rate = rospy.Rate(1/time_step)
    previous_trans = tf2_ros.TransformStamped()
    #========== deep deterministic policy gradient contorller
    brain = DDPG(batch_size = 1, batch_per_ep = 100)
    ep_rewards = []
    for i in range(brain.episodes): #for every episode
        total_reward = 0
        for j in range(brain.max_ep_length): #for every step
            if j == 0:#Go to start position on episode beginning           
                pub.publish(home_command())
                rospy.sleep(rospy.Duration(2))
                try: #to read tool position (to reward computation)
                    trans = tfBuffer.lookup_transform('world', 'tool0', rospy.Time())
                except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                    rate.sleep()
                    continue
                previous_trans = trans
                continue
            #any other step
            state = utils.state_joint(currentJointState)
            action = brain.get_action(state) #choose action
            pub.publish(vel_command(action[0], time_step=time_step))
            rospy.sleep(time_step)
            try: #to read tool position (to reward computation) 
                trans = tfBuffer.lookup_transform('world', 'tool0', rospy.Time())
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                rate.sleep()
                continue
            reward =get_reward(trans=trans, prev_trans=previous_trans)
            total_reward+=reward
            previous_trans = trans
            final = 1.0 * (reward < - 3.5) #is episode finished?
                #new_state = np.expand_dims(np.zeros(12), axis = 0) #dummy new state (will not be used)
            new_state = utils.state_joint(currentJointState)
            brain.memory.addMemory(state, action, reward, new_state, final)
            if final:
                break
        #train brain
        print 'Current memory size', brain.memory.getCurrentSize()
        if brain.memory.getCurrentSize() > brain.batch_size:
            critic_errors = []
            actor_errors = []
            for _ in range(brain.batch_per_ep):            
                batch = brain.memory.getMiniBatch(brain.batch_size)
                critic_loss, actor_loss = brain.process_batch(batch)
                critic_errors.append(critic_loss)
                actor_errors.append(actor_loss),
            print 'Episode: {}'.format(i)
            print '\tActor error: \t {0:.2f}'.format(np.mean(actor_errors))
            print '\tCritic error: \t {0:.2f}'.format(np.mean(critic_errors))
            print '\tEpisode reward: \t {0:2f}'.format(np.mean(total_reward))
        if i % 100 == 0:
            brain.backup_models(episode = i)
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import lasagne
import lasagne.layers as L
import theano
import numpy as np
import theano.tensor as T
import memory
import os

identity = lasagne.nonlinearities.identity
rectify = lasagne.nonlinearities.rectify
tanh = lasagne.nonlinearities.tanh
nesterov_mom = lasagne.updates.nesterov_momentum


'''
Actor critic architecture based on: http://pemami4911.github.io/blog/2016/08/21/ddpg-rl.html
and Lillicrap, Timothy P., et al. "Continuous control with deep reinforcement learning." 
    arXiv preprint arXiv:1509.02971 (2015).
Another useful link:
https://gist.github.com/ctmakro/df15fc53d482ae91e41387a0448c6384 (actor train well explained)
'''
class network:
    def __init__(self, 
             hidden1 = 64,#units in hidden layer
             hidden2 = 128, #units in hidden layer
             num_states = 12, #number of state descriptors
             num_actions = 6, #number of action output (6 vels of robot axis)
             batch_size = 16,
             learning_rate = 0.001, 
             states_var = None,
             action_var = None,
             target_var = None): #from temporal difference eqution):
        self.hidden1, self.hidden2 = hidden1, hidden2            
        self.num_states, self.num_actions = num_states, num_actions
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.states_var = states_var
        self.action_var = action_var
        self.target_var = target_var
        self.compile_functions()

    def build_network(self, states_var = None, *args):
        raise NotImplementedError
    def update_weights(self, other_network, tau = 0.001):
        #target network update
        params = L.get_all_param_values(self.model)
        other_params = L.get_all_param_values(other_network.model)
        new_params = [tau * other_params[i] + (1.0 - tau) * params[i] \
                      for i in range(len(params))]
        L.set_all_param_values(self.model, new_params)
    def load_weights(self, weights_file = './model.npz'):
        if os.path.exists(weights_file):
            base_data = np.load(weights_file)
            base_data = base_data[base_data.keys()[0]]
            L.set_all_param_values(self.model, base_data)
        else:
            print 'Model weights file does not exists'
    def save_weights(self,  name= 'model', episode = 0):
        np.savez(name + '_'+str(episode)+'.npz', L.get_all_param_values(self.model))
    def compile_functions(self):
        raise NotImplementedError
        
class actor_network(network):
    def build_network(self, states_var = None, *args):
        network = L.InputLayer(shape=(self.batch_size,self.num_states), input_var=states_var)
        network = L.DenseLayer(network, num_units=self.hidden1, nonlinearity=rectify)
        network = L.DenseLayer(network, num_units=self.hidden2, nonlinearity=rectify)
        network = L.DenseLayer(network, num_units=self.num_actions, nonlinearity=tanh,
                               W = lasagne.init.Normal(std = 0.01))
        #self.model = network
        return network
    def compile_functions(self):
        self.states_var = T.fmatrix('states')
        self.model = self.build_network(self.states_var)
        prediction = L.get_output(self.model)
        self.f = theano.function([self.states_var], prediction, 
                                 allow_input_downcast = True)     
    
class critic_network(network):
    def build_network(self, states_var = None, action_var = None):
        in_state = L.InputLayer(shape=(self.batch_size,self.num_states), input_var=states_var)
        in_action = L.InputLayer(shape=(self.batch_size,self.num_actions), input_var=action_var)
        network = L.DenseLayer(in_state, num_units=self.hidden1, nonlinearity=rectify)
        t1 = L.DenseLayer(network, num_units=self.hidden2, nonlinearity=rectify)
        t2 = L.DenseLayer(in_action, num_units=self.hidden2, nonlinearity=rectify)
        network = L.ConcatLayer([t1,t2], axis = 1)
        network = L.DenseLayer(network, num_units=1, nonlinearity=identity)
        return network, in_state, in_action
    def compile_functions(self):
        
        self.model, self.in_state, self.in_action = self.build_network(self.states_var, self.action_var)
        prediction = L.get_output(self.model)
        all_params = L.get_all_params(self.model, trainable=True) 
        self.f = theano.function([self.states_var, self.action_var], prediction,
                                 allow_input_downcast = True)
        loss = lasagne.objectives.squared_error(prediction, self.target_var)
        loss = loss.mean()
        updates = lasagne.updates.adam(loss, all_params, learning_rate=self.learning_rate)
        self.train_fn=theano.function([self.states_var, self.action_var, self.target_var],loss, 
                                   updates=updates, allow_input_downcast=True)

class DDPG:
    def __init__(self,
                 epsilon = 0.01, #exploration rate
                 gamma = 0.99, #discount rate
                 tau = 0.01, #target update rate
                 max_ep_length = 1000, #max episode length
                 target_update = 100,
                 memory_size = 1000, 
                 episodes = 5000, #num of episodes
                 batch_size = 16,
                 batch_per_ep = 100, #batches of training per episode
                 actor_class = actor_network,
                 critic_class = critic_network): 
        self.epsilon = epsilon
        self.gamma = gamma
        self.tau = tau
        self.max_ep_length = max_ep_length
        self.episodes = episodes
        self.memory = memory.Memory(memory_size)
        self.batch_per_ep = batch_per_ep
        self.batch_size = batch_size
        self.states_var = T.fmatrix('states')
        self.action_var = T.fmatrix('actions')
        self.target_var = T.fmatrix('TD') #from temporal difference eqution
        self.set_actor(actor_class)
        self.set_critic(critic_class)
        self.compile_functions()

        
    def set_actor(self, actor_class):
        self.online_actor = actor_class(batch_size = self.batch_size,
                                        states_var = self.states_var, 
                                        learning_rate = 0.0001)
        self.target_actor = actor_class(batch_size = self.batch_size,
                                        states_var = self.states_var)
        self.online_actor.load_weights('actor_model.npz')
        self.target_actor.load_weights('actor_model.npz')
    def set_critic(self, critic_class):
        self.online_critic = critic_class(batch_size = self.batch_size,
                                          states_var = self.states_var,
                                          action_var = self.action_var,
                                          target_var = self.target_var)
        self.target_critic = critic_class(batch_size = self.batch_size,
                                        states_var = self.states_var,
                                        action_var = self.action_var,
                                        target_var = self.target_var)
        self.online_critic.load_weights('critic_model.npz')
        self.target_critic.load_weights('critic_model.npz')
        
    def compile_functions(self):
        all_params = L.get_all_params(self.online_actor.model)
        pred_actions = L.get_output(self.online_actor.model, self.states_var)
        pred_q = L.get_output(self.target_critic.model, 
                              {self.target_critic.in_state:self.states_var, 
                               self.target_critic.in_action:pred_actions})
        pred_q = -pred_q.mean()
        updates = lasagne.updates.adam(pred_q, all_params, 
                                       learning_rate=self.online_actor.learning_rate)
        self.train_actor=theano.function([self.states_var],pred_q, 
                           updates=updates, allow_input_downcast=True)
        
    def process_batch(self, batch): 
        ''' batch is dictionary from memory package 
        https://github.com/vmayoral/basic_reinforcement_learning'''
        states = np.array([x['state'] for x in batch])[:,0,:]
        actions = np.array([x['action'] for x in batch])[:,0,:]
        new_states = np.array([x['newState'] for x in batch])[:,0,:]
        rewards = np.array([x['reward'] for x in batch])
        is_final = np.array([x['isFinal']for x in batch])
        #critic train
        new_actions = self.online_actor.f(new_states)
        targets = rewards + (1.0 - is_final) * (self.gamma * self.target_critic.f(new_states, new_actions))[:,0]
        targets = np.expand_dims(targets, axis =1)
        critic_loss = self.online_critic.train_fn(states, actions, targets)
        self.target_critic.update_weights(self.online_critic)
        #actor train
        actor_loss = self.train_actor(states)
        self.target_actor.update_weights(self.online_actor)
        return critic_loss, actor_loss
    
    def get_action(self, state):
        if np.random.rand() < self.epsilon:
            action = np.expand_dims(np.random.rand(6),axis = 0)
        else:
            action = self.target_actor.f(state)
        return action  
    def backup_models(self, episode = 0):
        self.target_actor.save_weights(name = 'actor', episode = episode)
        self.target_critic.save_weights(name = 'critic', episode = episode)
        
        
if __name__ == '__main__':
    brain = DDPG()
    #Fake training 
    ep_rewards = []
    for i in range(brain.episodes): #for every episode
        total_reward = 0
        state = np.expand_dims(np.random.rand(12), axis = 0) #read initial env state
        for _ in range(brain.max_ep_length): #for every step
            action = brain.online_actor.f(state) #choose action
            #env interaction here
            final = 1.0 * (np.random.randint(0,6) == 1) #is episode finished?
            if final:
                fake_state_t1 = np.expand_dims(np.zeros(12), axis = 0) #dummy new state (will not be used)
                break #if episode early stopped
            state_t1 = state + 0.1 *np.random.rand(12) #read new env state
            reward = 100 *(1-2*np.random.rand()) #read action reward
            total_reward+=reward
            brain.memory.addMemory(state, action, reward, state_t1, final)
            state = state_t1        
        #train brain
        critic_errors = []
        actor_errors = []
        for _ in range(brain.batch_per_ep):            
            batch = brain.memory.getMiniBatch(brain.batch_size)
            critic_loss, actor_loss = brain.process_batch(batch)
            critic_errors.append(critic_loss)
            actor_errors.append(actor_loss),
        print 'Episode: {}'.format(i)
        print '\tActor error: \t {0:.2f}'.format(np.mean(actor_errors))
        print '\tCritic error: \t {0:.2f}'.format(np.mean(critic_errors))
        print '\tEpisode reward: \t {0:2f}'.format(total_reward)

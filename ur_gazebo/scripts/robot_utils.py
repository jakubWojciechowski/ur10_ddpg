#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import tf2_ros
import numpy as np
def quaternion_multiply(quaternion0, quaternion1):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                     x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                     -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                     x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)
def quaternion_distance(quaternion0, quaternion1):
    #function returns distance between two quaternions (in radians)
    inv_quaternion0 = np.array(quaternion0) * np.array([1,-1,-1,-1])
    diff = quaternion_multiply(inv_quaternion0, quaternion1)
    diff_angle = np.arccos(np.clip(diff[0],0,1))
    return diff_angle

def pos(trans = tf2_ros.TransformStamped()):
    return np.array([trans.transform.translation.x,
                     trans.transform.translation.y,
                     trans.transform.translation.z])
    
def rot(trans = tf2_ros.TransformStamped()):
    return np.array([trans.transform.rotation.w,
                     trans.transform.rotation.x,
                     trans.transform.rotation.y,
                     trans.transform.rotation.z])
def time(trans = tf2_ros.TransformStamped()):
    return trans.header.stamp

def start_joint_pos():
    position = np.zeros(6)
    position[0] = 0
    position[1] = -np.pi/2
    position[2] = np.pi/2 
    position[3] = 0
    position[4] = np.pi/2
    position[5] = -np.pi/2
    return position

def start_tool0_orientation():
    return np.array([0.70711,0.0,0.70711,0.0])

def state_joint(joint_state):
    state = dict(zip(joint_state.name, joint_state.position))
    target_names = ur10_joint_names()
    state_sorted = [state[x] for x in target_names]
    states = [[np.sin(x), np.cos(x)] for x in state_sorted]
    states = np.reshape(states, (1,12))
    return states
    
def ur10_joint_names(): 
    return ['shoulder_pan_joint', 'shoulder_lift_joint', 
            'elbow_joint', 'wrist_1_joint', 'wrist_2_joint', 
            'wrist_3_joint']


